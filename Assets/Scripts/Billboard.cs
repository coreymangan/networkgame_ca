﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Billboard : NetworkBehaviour {
    public new Camera camera;

    private void Awake()
    {
        this.camera.enabled = false;
    }

    // Update is called once per frame
    void Update () {
        if (!isLocalPlayer)
        {
            return;
        }

        transform.LookAt(camera.transform);
	}

    public override void OnStartLocalPlayer()
    {
        camera.enabled = true;
    }
}
