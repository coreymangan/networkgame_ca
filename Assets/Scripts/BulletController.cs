﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour {
    private int bulletDamage = 10;

    void OnCollisionEnter(Collision collision)
    {
        var hit = collision.gameObject;
        var health = hit.GetComponent<HealthController>();
        if (health != null)
        {
            health.TakeDamage(bulletDamage);
        }
        Destroy(gameObject);
    }
}
