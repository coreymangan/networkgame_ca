﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class EnemySpawner : NetworkBehaviour {
    public GameObject enemyprefab;
    private int numEnemies = 6;
    private float spawnRange = 15.0f;

    public override void OnStartServer()
    {
        for (int i = 0; i < numEnemies; i++)
        {
            var spawnPos = new Vector3(Random.Range(-spawnRange, spawnRange), 1.0f, Random.Range(-spawnRange, spawnRange));
            var spawnRot = Quaternion.Euler(0.0f, Random.Range(0, 180), 0.0f);

            var enemy = (GameObject)Instantiate(enemyprefab, spawnPos, spawnRot);
            NetworkServer.Spawn(enemy);
        }
    }
}
