﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerController : NetworkBehaviour {
    private float verSpeed = 5.0f;
    private float horSpeed = 50.0f;
    private float bulletSpeed = 6.0f;
    private Animator anim;
    private NetworkAnimator netAnim;
    public GameObject bulletPrefab;
    public Transform bulletSpawn;
    public Camera camera;

	// Use this for initialization
	void Awake () {
        camera.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (!isLocalPlayer)
        {
            return;
        }

        // Moving
        float ver = Input.GetAxis("Vertical") * Time.deltaTime * verSpeed;
        float hor = Input.GetAxis("Horizontal") * Time.deltaTime * horSpeed;

        this.anim.SetFloat("verSpeed", ver);

        transform.Rotate(0, hor, 0);
        transform.Translate(0, 0, ver);

        // Firing
        if (Input.GetKeyDown(KeyCode.Space)) // && Time.time > fireRate + lastShot)
        {
            this.netAnim.SetTrigger("fire");
            if (NetworkServer.active)
            {
                this.anim.ResetTrigger("fire");
            }
           // CmdAttackAnim();
        }
	}

    /*[Command]
    void CmdAttackAnim()
    {
        RpcSetTrigger("fire");
    }

    [ClientRpc]
    public void RpcSetTrigger(string trigger)
    {
        this.netAnim.SetTrigger(trigger);
    }*/

    // Fire bullets
    [Command]
    void CmdFire()
    {
        // Spawn bullet
        var bullet = (GameObject)Instantiate(bulletPrefab, bulletSpawn.position, bulletSpawn.rotation);

        // Add velocity
        bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * bulletSpeed;

        // Spawn bullet on client
        NetworkServer.Spawn(bullet);

        // Destroy bullet after 2secs
        Destroy(bullet, 2.0f);

    }

    public override void OnStartLocalPlayer()
    {
        // GetComponent<Renderer>().material.color = Color.blue;
        camera.enabled = true;
        this.anim = GetComponent<Animator>();
        this.netAnim = GetComponent<NetworkAnimator>();
    }

    
}
